package products;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {

        Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
        Menu menuWindow = new Menu();
        menuWindow.menu();
        menuWindow.hibernateProductRepository.entityManager.close();
    }
}

package products;

import java.util.List;

public interface ProductRepository {
    public void addProduct(Products product);

    public List<Products> getProducts();

    public void editProduct(Products product);

    public void deleteProduct(int id);

    public Products selectProductById(int id);
}

package products;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class HibernateProductRepository implements ProductRepository {
    EntityManagerFactory factory =
            Persistence.createEntityManagerFactory("products");
    EntityManager entityManager = factory.createEntityManager();

    public void addProduct(Products product) {
        entityManager.getTransaction().begin();
        entityManager.persist(product);
        entityManager.getTransaction().commit();
    }

    public List<Products> getProducts() {
        List<Products> products = entityManager
                .createQuery("from Products",Products.class)
                .getResultList();
        return products;
    }

    public void editProduct(Products product) {
        entityManager.getTransaction().begin();
        entityManager.merge(product);
        entityManager.getTransaction().commit();
    }

    public void deleteProduct(int id) {
        entityManager.getTransaction().begin();
        Products product = entityManager.find(Products.class,id);
        entityManager.remove(product);
        entityManager.getTransaction().commit();
    }

    @Override
    public Products selectProductById(int id) {
        return entityManager.find(Products.class,id);
    }
}

package products;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "product")
public class Products {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    String category;
    BigDecimal price;
    Integer rate;
    String description;

    @Override
    public String toString() {
        return "Products{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", price=" + price +
                ", rate=" + rate +
                ", description='" + description + '\'' +
                '}';
    }
}

package products;

import java.math.BigDecimal;
import java.util.Scanner;

public class Menu {
    Products product = new Products();
    HibernateProductRepository hibernateProductRepository = new HibernateProductRepository();

    public void menu() {
        System.out.println("Witaj drogi użytkowniku!");
        System.out.println("Powiedz nam co chcesz dzisiaj zrobić?");

        System.out.println("1. Dodaj produkt");
        System.out.println("2. Wyświetl produkty");
        System.out.println("3. Edytuj produkt");
        System.out.println("4. Usuń produkt");
        System.out.println("0. Zakończ pracę");

        Scanner input = new Scanner(System.in);
        int decision = input.nextInt();

        switch (decision) {

            case 1:
                hibernateProductRepository.addProduct(setProductParameters());
                break;
            case 2:
                hibernateProductRepository.getProducts().forEach(System.out::println);
                break;
            case 3:
                hibernateProductRepository.editProduct(selectProductToEdit());
                break;
            case 4:
                System.out.println("Podaj id produktu który chcesz usunąć: ");
                hibernateProductRepository.deleteProduct(input.nextInt());
                break;
            case 0:
                System.out.println("Koniec pracy");
                break;
            default:
                System.out.println("Złe polecenie");
                menu();
        }
    }

    public Products setProductParameters() {
        Scanner parameter = new Scanner(System.in);
        System.out.println("Podaj nazwę produktu: ");
        product.name = parameter.nextLine();
        System.out.println("Podaj kategorię produktu: ");
        product.category = parameter.nextLine();
        System.out.println("Podaj cenę produktu: ");
        product.price = parameter.nextBigDecimal();
        System.out.println("Podaj ocenę produktu: ");
        product.rate = parameter.nextInt();
        parameter.nextLine();
        System.out.println("Podaj opis produktu: ");
        product.description = parameter.nextLine();
        return product;
    }

    public Products selectProductToEdit() {
        System.out.println("Lista dostępnych produktów");
        hibernateProductRepository.getProducts().forEach(System.out::println);
        Scanner parameter = new Scanner(System.in);
        System.out.println("Podaj id produktu do edycji: ");
        product.id = parameter.nextInt();
        product = hibernateProductRepository.selectProductById(product.id);
        parameter.nextLine();

        System.out.println("Podaj nazwę produktu: ");
        String newValue = parameter.nextLine();
        if (!"".equals(newValue)) {
            product.name = newValue;
        }
        System.out.println("Podaj kategorię produktu: ");
        newValue = parameter.nextLine();
        if (!"".equals(newValue)) {
            product.category = newValue;
        }
        System.out.println("Podaj cenę produktu: ");
        newValue = parameter.nextLine();
        if (!"".equals(newValue)) {
            product.price = BigDecimal.valueOf(Double.parseDouble(newValue));
        }
        System.out.println("Podaj ocenę produktu: ");
        newValue = parameter.nextLine();
        if (!"".equals(newValue)) {
            product.rate = Integer.parseInt(newValue);
        }
        System.out.println("Podaj opis produktu: ");
        newValue = parameter.nextLine();
        if (!"".equals(newValue)) {
            product.description = newValue;
        }
        return product;
    }
}
